import dict_tools
import pytest


@pytest.mark.asyncio
async def test_gather_no_profiles(hub, ctx):
    hub.OPT = dict_tools.data.NamespaceDict(
        {
            "idem": {"acct_profile": "test-profile"},
        }
    )
    ret = await hub.acct.k8s.init.gather(profiles={})
    assert "test-profile" in ret
